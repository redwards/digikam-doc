# Translation of docs_digikam_org_supported_materials___camera_devices.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-10-31 10:07+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../supported_materials/camera_devices.rst:1
msgid "Camera Devices Supported by digiKam"
msgstr "Dispositius de càmera admesos pel digiKam"

#: ../../supported_materials/camera_devices.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, camera, gphoto, usb, mass, storage"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, càmera, gphoto, usb, massiu, "
"emmagatzematge"

#: ../../supported_materials/camera_devices.rst:14
msgid "Camera Devices"
msgstr "Dispositius de càmera"

#: ../../supported_materials/camera_devices.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../supported_materials/camera_devices.rst:19
msgid "Introduction"
msgstr "Introducció"

#: ../../supported_materials/camera_devices.rst:23
msgid ""
"RAW file support: if you are using RAW shooting mode with your camera, "
"digiKam is probably well able to deal with it. RAW support depends on the "
"libraw library. To find out if your particular camera is supported, bring up "
"the list from the :menuselection:`Help --> Supported RAW Cameras` menu."
msgstr ""
"Compatibilitat amb els fitxers RAW: si utilitzeu el mode de captura RAW amb "
"la vostra càmera, és probable que el digiKam pugui manejar-lo. El suport RAW "
"depèn de la biblioteca LibRaw. Per a esbrinar si la vostra càmera és "
"compatible, obriu la llista des de l'element de menú «:menuselection:`Ajuda "
"--> Càmeres RAW admeses`»."

#: ../../supported_materials/camera_devices.rst:25
msgid ""
"How to setup and work with RAW files is described in :ref:`RAW Decoding "
"Settings <setup_raw>` and :ref:`RAW Workflow <rawprocessing_workflow>` "
"sections."
msgstr ""
"La manera de configurar i treballar amb els fitxers RAW es descriu a les "
"seccions :ref:`Configuració de la descodificació RAW <setup_raw>` i :ref:"
"`Flux de treball RAW <rawprocessing_workflow>`."

#: ../../supported_materials/camera_devices.rst:29
msgid ""
"Camera import is currently not supported on Windows Systems due limitations "
"in the Gphoto2 library."
msgstr ""
"Actualment, la importació de càmera no està implementada als sistemes "
"Windows degut a limitacions en la biblioteca Gphoto2."

# skip-rule: t-acc_obe
#: ../../supported_materials/camera_devices.rst:31
msgid ""
"An easy-to-use camera interface is provided that will connect to your "
"digital camera and download photographs directly into digiKam Albums. More "
"than `2500 digital cameras <http://www.gphoto.org/proj/libgphoto2/support."
"php>`_ are supported by the gPhoto library. Of course, any media or card "
"reader supported by your operating system will interface with digiKam."
msgstr ""
"Es proporciona una interfície de càmera fàcil d'usar que es connectarà amb "
"la càmera digital i descarregarà les fotografies directament cap als àlbums "
"del digiKam. La biblioteca gPhoto admet més de `2.500 càmeres digitals "
"<http://www.gphoto.org/proj/libgphoto2/support.php>`_. Per descomptat, "
"qualsevol suport o lector de targetes compatible amb el sistema operatiu es "
"connectarà amb el digiKam."

#: ../../supported_materials/camera_devices.rst:33
msgid ""
"Current digital cameras are characterized by the use of Compact Flash™ "
"Memory cards and USB or FireWire (IEEE-1394 or i-link) for data "
"transmission. The actual transfers to a host computer are commonly carried "
"out using the USB Mass Storage device class (so that the camera appears as a "
"disk drive) or using the Picture Transfer Protocol (PTP) and its "
"derivatives. Older cameras may use the Serial Port (RS-232) connection."
msgstr ""
"Les càmeres digitals actuals es caracteritzen per l'ús de targetes Compact "
"Flash™ Memory, USB o FireWire (IEEE-1394 o i-Link) per a la transmissió de "
"les dades. Les transferències reals a un ordinador central, habitualment es "
"duen a terme, utilitzant una classe de dispositiu d'emmagatzematge massiu "
"USB (de manera que la càmera apareixerà com una unitat de disc) o utilitzant "
"el protocol per a la transferència d'imatges (Picture Transfer Protocol -"
"PTP-) i els seus derivats. Les càmeres antigues poden utilitzar la connexió "
"del port sèrie (RS-232)."

#: ../../supported_materials/camera_devices.rst:36
msgid "Transfers using gPhoto: PTP and Serial Port"
msgstr "Transferències mitjançant el gPhoto: PTP i port sèrie"

#: ../../supported_materials/camera_devices.rst:38
msgid ""
"digiKam employs the gPhoto program to communicate with digital still "
"cameras. Gphoto is a free, redistributable set of digital camera software "
"applications which supports a growing number of cameras. Gphoto has support "
"for the Picture Transfer Protocol, which is a widely supported protocol "
"developed by the International Imaging Industry Association to allow the "
"transfer of images from digital cameras to computers and other peripheral "
"devices without the need of additional device drivers."
msgstr ""
"El digiKam utilitza el programa gPhoto per a comunicar-se amb les càmeres "
"fotogràfiques digitals. El gPhoto és un conjunt lliure i redistribuïble "
"d'aplicacions de programari per a càmeres digitals que admet un nombre "
"creixent de càmeres. El gPhoto és compatible amb el protocol per a la "
"transferència d'imatges, el qual és un protocol àmpliament admès "
"desenvolupat per l'Associació Internacional de la Indústria d'Imatges per a "
"permetre la transferència d'imatges des de càmeres digitals cap a ordinadors "
"i altres dispositius perifèrics sense necessitat de controladors de "
"dispositiu addicionals."

# skip-rule: t-acc_obe
#: ../../supported_materials/camera_devices.rst:40
msgid ""
"Many old digital still cameras used Serial Port to communicate with host "
"computers. Because photographs are big files and serial port transfers are "
"slow, this connection is now obsolete. digiKam supports these cameras and "
"performs image transfers using the gPhoto program. You can find a complete "
"list of supported digital cameras at `this url <http://www.gphoto.org/proj/"
"libgphoto2/support.php>`_."
msgstr ""
"Moltes càmeres fotogràfiques digitals antigues feien servir el port sèrie "
"per a comunicar-se amb els ordinadors amfitrions. Com que les fotografies "
"són fitxers grans i les transferències del port sèrie són lentes, aquesta "
"connexió ara està obsoleta. El digiKam admet aquestes càmeres i fa "
"transferències d'imatges mitjançant el programa gPhoto. Trobareu una llista "
"completa de càmeres digitals admeses en `aquest URL <http://www.gphoto.org/"
"proj/libgphoto2/support.php>`_."

#: ../../supported_materials/camera_devices.rst:44
msgid ""
"Gphoto needs to be built with libexif to retrieve thumbnails to digiKam "
"properly. Exif support is required for thumbnail retrieval on some "
"libgphoto2 camera drivers. If Exif support is not set with gPhoto, you might "
"not see thumbnails or the thumbnail extraction may be very slow."
msgstr ""
"El gPhoto s'ha de construir amb la libexif per a rebre correctament les "
"miniatures al digiKam. Cal compatibilitat amb Exif per a la recuperació de "
"miniatures en alguns controladors de càmera de la libgphoto2. Si la "
"compatibilitat amb Exif no està establerta amb el gPhoto, és possible que no "
"veieu les miniatures o que l'extracció d'aquestes sigui molt lenta."

#: ../../supported_materials/camera_devices.rst:50
msgid "The digiKam Setup Dialog to Configure a gPhoto Camera Device"
msgstr ""
"El diàleg de configuració del digiKam per a configurar un dispositiu de "
"càmera de gPhoto"

#: ../../supported_materials/camera_devices.rst:53
msgid "Transfers using Mass Storage device"
msgstr "Transferències emprant un dispositiu d'emmagatzematge massiu"

#: ../../supported_materials/camera_devices.rst:55
msgid ""
"For the devices that are not directly supported by gPhoto, there is support "
"for the Mass Storage protocol, which is well supported under GNU/Linux®. "
"This includes many digital cameras and Memory Card Readers. Mass Storage "
"interfaces are:"
msgstr ""
"Per als dispositius que no són admesos directament pel gPhoto, hi ha suport "
"per al protocol d'emmagatzematge massiu, compatible amb GNU/Linux®. Això "
"inclou moltes càmeres digitals i lectors de targetes de memòria. Les "
"interfícies d'emmagatzematge massiu són:"

#: ../../supported_materials/camera_devices.rst:57
msgid ""
"**USB Mass Storage**: a computer interface using communication protocols "
"defined by the USB Implementers Forum that run on the Universal Serial Bus. "
"This standard provides an interface to a variety of storage devices, "
"including digital cameras."
msgstr ""
"**Emmagatzematge massiu USB**: una interfície d'ordinador que utilitza "
"protocols de comunicació definits pel Fòrum d'implementadors d'USB que "
"s'executen sobre el bus sèrie universal. Aquest estàndard proporciona una "
"interfície per a una varietat de dispositius d'emmagatzematge, incloses les "
"càmeres digitals."

#: ../../supported_materials/camera_devices.rst:59
msgid ""
"**FireWire Mass Storage**: a computer interface using communication "
"protocols developed primarily by Apple Computer in the 1990s. FireWire "
"offers high-speed communications and isochronous real-time data services. "
"Like USB Mass Storage, this standard provides an interface to a variety of "
"storage devices, including digital still cameras. Almost all recent digital "
"cameras support USB version 1 and eventually will support USB version 2; a "
"very few support FireWire."
msgstr ""
"**Emmagatzematge massiu FireWire**: una interfície d'ordinador que utilitza "
"protocols de comunicació desenvolupats principalment per Apple Computer a la "
"dècada de 1990. El FireWire ofereix comunicacions d'alta velocitat i serveis "
"de dades isòcrones en temps real. Igual que l'emmagatzematge massiu USB, "
"aquest estàndard proporciona una interfície per a una varietat de "
"dispositius d'emmagatzematge, incloses les càmeres fotogràfiques digitals. "
"Gairebé totes les càmeres digitals recents admeten la versió 1 d'USB i "
"finalment admetran la versió 2, molt poques admeten el FireWire."

#: ../../supported_materials/camera_devices.rst:61
msgid ""
"To use a generic Mass Storage device with digiKam, select :menuselection:"
"`Import --> Camera --> Add Camera Manually...`, add your device and set the "
"correct mount point path."
msgstr ""
"Per a utilitzar un dispositiu d'emmagatzematge massiu genèric amb el "
"digiKam, seleccioneu l'element de menú «:menuselection:`Importa --> Càmera --"
"> Afegeix una càmera manualment...`», afegiu el dispositiu i establiu el "
"camí del punt de muntatge correcte."

#: ../../supported_materials/camera_devices.rst:63
msgid ""
"For details see the chapter to configure digiKam, into :ref:`Camera section "
"<camera_settings>`."
msgstr ""
"Per a més detalls, vegeu el capítol per a configurar el digiKam a la :ref:"
"`secció Càmera <camera_settings>`."
