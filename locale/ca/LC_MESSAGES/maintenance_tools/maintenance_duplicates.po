# Translation of docs_digikam_org_maintenance_tools___maintenance_duplicates.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-05-12 00:16+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.07.70\n"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid "digiKam Maintenance Tool to Find Duplicates"
msgstr "L'eina de manteniment Cerca les duplicades en el digiKam"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, duplicates, similarity"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, manteniment, duplicats, similitud"

#: ../../maintenance_tools/maintenance_duplicates.rst:14
msgid "Find Duplicates"
msgstr "Trobar les duplicades"

#: ../../maintenance_tools/maintenance_duplicates.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../maintenance_tools/maintenance_duplicates.rst:22
msgid "The digiKam Maintenance Options to Find Duplicates"
msgstr "Les opcions de manteniment per a «Cerca les duplicades» en el digiKam"

#: ../../maintenance_tools/maintenance_duplicates.rst:24
msgid ""
"The **Find Duplicates** Tool is doing the same as the Find duplicates button "
"in the :ref:`the Similarity View <similarity_view>`, but here you can "
"combine it with other maintenance operations and you have the chance to "
"check **Work on all processor cores** under :ref:`Common Options "
"<maintenance_common>` to speed up the process."
msgstr ""
"L'eina **Cerca les duplicades** està fent el mateix que el botó Cerca les "
"duplicades a :ref:`la vista de similitud <similarity_view>`, però aquí "
"podreu combinar-la amb altres operacions de manteniment i teniu "
"l'oportunitat de marcar **Treballa amb tots els nuclis dels processadors** "
"sota :ref:`Opcions comunes <maintenance_common>` per a accelerar el procés."

#: ../../maintenance_tools/maintenance_duplicates.rst:30
msgid "The digiKam Find Duplicates Button from Similarity Left Sidebar"
msgstr ""
"El botó «Cerca les duplicades» des de la barra lateral esquerra Similitud en "
"el digiKam"

#: ../../maintenance_tools/maintenance_duplicates.rst:32
msgid "This process provides two options to find duplicates items:"
msgstr ""
"Aquest procés proporciona dues opcions per a trobar els elements duplicats:"

#: ../../maintenance_tools/maintenance_duplicates.rst:34
msgid ""
"**Similarity Range**: the lower and higher values to define the range of "
"similarity in percents."
msgstr ""
"**Interval de similitud**: els valors més baixos i més alts per a definir "
"l'interval de similitud en percentatges."

#: ../../maintenance_tools/maintenance_duplicates.rst:36
msgid ""
"**Restriction**: this option restrict the duplicate search with some "
"criteria, as to limit search to the album of reference image, or to exclude "
"the album of reference image of the search."
msgstr ""
"**Restricció**: aquesta opció restringeix la cerca de duplicats amb alguns "
"criteris, com ara limitar la cerca a l'àlbum de la imatge de referència, o "
"excloure aquest àlbum."

#: ../../maintenance_tools/maintenance_duplicates.rst:38
msgid ""
"While the find duplicates process is under progress, notification on the "
"bottom right of main windows will be visible to indicate the amount of items "
"already done."
msgstr ""
"Mentre el procés de trobar les duplicades estigui en progrés, es veurà una "
"notificació a la part inferior dreta de la finestra principal per a indicar "
"la quantitat d'elements que ja s'han fet."

#: ../../maintenance_tools/maintenance_duplicates.rst:44
msgid "The digiKam Find Duplicates Process Running in the Background"
msgstr "El procés de Cerca les duplicades en el digiKam treballa en segon pla"
