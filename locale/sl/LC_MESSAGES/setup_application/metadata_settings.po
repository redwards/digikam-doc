# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-10-28 09:54+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.4\n"

#: ../../setup_application/metadata_settings.rst:1
msgid "digiKam Metadata Settings"
msgstr "digiKam nastavitve metapodatkov"

#: ../../setup_application/metadata_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy"
msgstr ""
"digiKam, dokumentacija, uporabniški priročnik, upravljanje fotografij, "
"odprta koda, prosto, učenje, enostavno"

#: ../../setup_application/metadata_settings.rst:14
msgid "Metadata Settings"
msgstr "Nastavitve metapodatkov"

#: ../../setup_application/metadata_settings.rst:16
msgid "Contents"
msgstr "Vsebina"

#: ../../setup_application/metadata_settings.rst:18
msgid ""
"Image files can have some metadata embedded into the image file format. "
"These metadata can be stored in a number of standard formats as JPEG, TIFF, "
"PNG, JPEG2000, PGF, and RAW files. Metadata can be read and written in the "
"`Exif <https://en.wikipedia.org/wiki/Exif>`_, `IPTC <https://en.wikipedia."
"org/wiki/IPTC_Information_Interchange_Model>`_, and `XMP <https://en."
"wikipedia.org/wiki/Extensible_Metadata_Platform>`_ formats if they are "
"present in the file."
msgstr ""
"Slikovne datoteke imajo lahko nekaj metapodatkov, vdelanih v obliko slikovne "
"datoteke.Te metapodatke je mogoče shraniti v številnih standardnih formatih, "
"kot so datoteke JPEG, TIFF, PNG, JPEG2000, PGF in RAW. Metapodatke je mogoče "
"prebrati in zapisati v `Exif <https://en.wikipedia.org/wiki/Exif>`_, `IPTC "
"<https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model>`_ in `XMP "
"<https://en.wikipedia.org/wiki/Extensible_Metadata_Platform>`_ formatih, če "
"so prisotni v datoteki."

#: ../../setup_application/metadata_settings.rst:20
msgid ""
"Storing in metadata allows one to preserve this information when moving or "
"sending the files to different systems."
msgstr ""
"Shranjevanje v metapodatke omogoča ohranjanju teh informacij pri premikanju "
"ali pošiljanju datotek v različne sisteme."

#: ../../setup_application/metadata_settings.rst:25
msgid "Behavior Settings"
msgstr "Nastavitve vedenja"

#: ../../setup_application/metadata_settings.rst:27
msgid ""
"The **Behavior** tab allows you to select what information digiKam will "
"write to the metadata and control how digiKam will deal with this embedded "
"information."
msgstr ""
"Zavihek **Vedenje** vam omogoča, da izberete, katere informacije bo digiKam "
"zapisal v metapodatke, in nadzirate, kako bo digiKam ravnal s temi vdelanimi "
"informacijami."

#: ../../setup_application/metadata_settings.rst:33
msgid "The digiKam Metadata General Behavior Settings Page"
msgstr "Stran s splošnimi nastavitvami metapodatkov digiKam"

#: ../../setup_application/metadata_settings.rst:35
msgid ""
"The options available in the **Write This Information to the Metadata** "
"section are listed below:"
msgstr ""
"Možnosti, ki so na voljo v razdelku **Zapiši te informacije v metapodatke**, "
"so navedene spodaj:"

#: ../../setup_application/metadata_settings.rst:37
msgid ""
"**Image tags** will store the tag paths and keywords used to mark the "
"contents. Usually this information is stored in IPTC and XMP."
msgstr ""
"**Oznake slik** bodo shranile poti oznak in ključne besede, uporabljene za "
"označevanje vsebine. Običajno so te informacije shranjene v IPTC in XMP."

#: ../../setup_application/metadata_settings.rst:38
msgid ""
"**Captions and titles** will store the internationalized strings used to "
"describe the contents. Usually this information is stored in Exif, IPTC, and "
"XMP."
msgstr ""
"**Napisi in naslovi** bodo shranili internacionalizirane nize, ki se "
"uporabljajo za opis vsebine. Običajno so te informacije shranjene v Exif, "
"IPTC in XMP."

#: ../../setup_application/metadata_settings.rst:39
msgid ""
"**Rating** will store the rate of the contents. Usually this information is "
"stored in Exif and XMP."
msgstr ""
"**Časovni žigi** bodo shranili datume vsebine. Običajno so te informacije "
"shranjene v Exif in XMP."

#: ../../setup_application/metadata_settings.rst:40
msgid ""
"**Pick label** will store the quality of the contents. Usually this "
"information is stored in XMP."
msgstr ""
"**Izberi oznako** bo shranila kakovost vsebine. Običajno so te informacije "
"shranjene v XMP."

#: ../../setup_application/metadata_settings.rst:41
msgid ""
"**Color label** will store the color flag used to classify your contents "
"while your workflow. Usually this information is stored in XMP."
msgstr ""
"**Barvna oznaka** bo shranila barvno zastavico, ki se uporablja za "
"razvrščanje vaše vsebine med potekom dela. Običajno so te informacije "
"shranjene v XMP."

#: ../../setup_application/metadata_settings.rst:42
msgid ""
"**Timestamps** will store the dates of the contents. Usually this "
"information is stored in Exif, IPTC, and XMP."
msgstr ""
"**Časovni žigi** bodo shranili datume vsebine. Običajno so te informacije "
"shranjene v Exif, IPTC in XMP."

#: ../../setup_application/metadata_settings.rst:43
msgid ""
"**Metadata templates (Copyrights etc.)**: will store the set of "
"internationalized strings used by the agencies workflow. Usually this "
"information is stored in IPTC and XMP."
msgstr ""
"**Predloge metapodatkov (avtorske pravice itd.)**: bodo shranile nabor "
"internacionaliziranih nizov, ki jih uporablja potek dela agencij. Običajno "
"so te informacije shranjene v IPTC in XMP."

#: ../../setup_application/metadata_settings.rst:44
msgid ""
"**Face Tags (including face areas)**: will store the face tag paths and the "
"rectangles corresponding to the zones around faces. Usually this information "
"is stored in XMP."
msgstr ""
"**Oznake obrazov (vključno z območji obrazov**: shranijo poti oznak obrazov "
"in pravokotnike, ki ustrezajo območjem okoli obrazov. Običajno so te "
"informacije shranjene v XMP."

#: ../../setup_application/metadata_settings.rst:45
msgid ""
"**Geolocation information (GPS)**: will store the world map position of the "
"contents. Usually this information is stored in Exif and XMP."
msgstr ""
"**Geolokacijski podatki (GPS)**: shranijo položaj vsebine na zemljevidu "
"sveta. Običajno so te informacije shranjene v Exif in XMP."

#: ../../setup_application/metadata_settings.rst:47
msgid ""
"The options available in the **Reading and Writing Metadata** section are "
"listed below:"
msgstr ""
"Možnosti, ki so na voljo v razdelku **Branje in pisanje metapodatkov**, so "
"navedene spodaj:"

#: ../../setup_application/metadata_settings.rst:49
msgid ""
"**Delegate to ExifTool backend all operations to write metadata to files** "
"allows to write metadata to files with the `ExifTool <https://exiftool.org/"
">`_ backend instead `Exiv2 <https://exiv2.org/>`_. This last one is primary "
"used for all operations on metadata. Using ExifTool instead will slowdown a "
"little bit the synchronization of files metadata with database."
msgstr ""
"**Delegiraj v zaledje ExifTool vse operacije za pisanje metapodatkov v "
"datoteke** omogoča pisanje metapodatkov v datoteke z zaledjem `ExifTool "
"<https://exiftool.org/>`_ namesto z `Exiv2 <https://exiv2.org/ >`_. To "
"zadnje se primarno uporablja za vse operacije z metapodatki. Če namesto tega "
"uporabite ExifTool, boste nekoliko upočasnili sinhronizacijo metapodatkov "
"datotek z bazo podatkov."

#: ../../setup_application/metadata_settings.rst:50
msgid ""
"**Write metadata to DNG files** allows to delegate to ExifTool all "
"operations to write metadata into DNG files."
msgstr ""
"**Zapisovanje metapodatkov v datoteke DNG** omogoča prenos vseh operacij za "
"zapisovanje metapodatkov v datoteke DNG na ExifTool."

#: ../../setup_application/metadata_settings.rst:51
msgid ""
"**If possible write metadata to RAW files** allows to delegate to ExifTool "
"all operations to write metadata into RAW files. This feature is disabled by "
"default."
msgstr ""
"**Če je mogoče, zapiši metapodatke v datoteke RAW** omogoča prenos vseh "
"operacij za zapisovanje metapodatkov v datoteke RAW na ExifTool. Ta funkcija "
"je privzeto onemogočena."

#: ../../setup_application/metadata_settings.rst:55
msgid ""
"See also the ExifTool backend configuration from the :ref:`ExifTool Settings "
"<metadata_exiftool>` section of this manual, and the `ExifTool write "
"limitations <https://exiftool.org/#limitations>`_."
msgstr ""
"Oglejte si tudi konfiguracijo zaledja ExifTool iz razdelka :ref:`Nastavitve "
"ExifTool <metadata_exiftool>` tega priročnika in `Omejitve pisanja ExifTool "
"<https://exiftool.org/#limitations>`_."

#: ../../setup_application/metadata_settings.rst:57
msgid "On the bottom, a section group extra behavior settings:"
msgstr "Na dnu razdelka združuje dodatne nastavitve vedenja:"

#: ../../setup_application/metadata_settings.rst:59
msgid ""
"**Use Lazy Synchronization** allows to schedule metadata for synchronization "
"instead to flush immediately. digiKam will only write metadata when user "
"clicks on the **Apply Pending Changes To Metadata** icon in the status bar "
"or when application is shutdown."
msgstr ""
"**Uporabi leno sinhronizacijo** omogoča načrtovanje metapodatkov za "
"sinhronizacijo namesto takojšnjega izpiranja. digiKam bo zapisal metapodatke "
"samo, ko uporabnik klikne ikono **Uveljavi čakajoče spremembe na "
"metapodatkih** v statusni vrstici ali ko se aplikacija zaustavi."

#: ../../setup_application/metadata_settings.rst:60
msgid ""
"**Update file modification timestamp when files are modified** allows to "
"update file timestamps when files are changed as when you update metadata or "
"image data. Note: disabling this option can introduce some dysfunctions with "
"external applications which use file timestamp properties to detect file "
"modifications automatically."
msgstr ""
"**Posodobi časovni žig spremembe datoteke, ko so datoteke spremenjene** "
"omogoča posodobitev časovnih žigov datotek, ko so datoteke spremenjene, kot "
"ko posodobite metapodatke ali slikovne podatke. Opomba: onemogočanje te "
"možnosti lahko povzroči nekatere motnje v delovanju zunanjih aplikacij, ki "
"uporabljajo lastnosti časovnega žiga datoteke za samodejno zaznavanje "
"sprememb datoteke."

#: ../../setup_application/metadata_settings.rst:61
msgid ""
"**Rescan file when files are modified** allows to force digiKam to rescan "
"files that has been modified outside the application. If a file has changed "
"it is file size or if the last modified timestamp has changed, a rescan of "
"that file will be performed when digiKam starts."
msgstr ""
"**Ponovno skeniraj datoteko, ko so datoteke spremenjene** omogoča, da "
"digiKam prisili, da ponovno prečeše datoteke, ki so bile spremenjene zunaj "
"aplikacije. Če je datoteka spremenila velikost datoteke ali če se je "
"spremenil zadnji spremenjeni časovni žig, bo ob zagonu digiKama izvedeno "
"ponovno skeniranje te datoteke."

#: ../../setup_application/metadata_settings.rst:66
msgid "Sidecars Settings"
msgstr "Nastavitve priklopnih datotek"

#: ../../setup_application/metadata_settings.rst:68
msgid ""
"The **Sidecars** tab allows user to control whether digiKam will read and "
"write from/to XMP sidecars or not. You can also customize the granularity of "
"write operations to XMP sidecar:"
msgstr ""
"Zavihek **Priklopne datoteke** omogoča uporabniku nadzor nad tem, ali bo "
"digiKam prebral in pisal iz/v priklopne datoteke XMP ali ne. Prilagodite "
"lahko tudi razdrobljenostzapisovanje operacij v priklopne datoteke XMP:"

#: ../../setup_application/metadata_settings.rst:70
msgid "**Write to XMP sidecar only** will not touch the item metadata."
msgstr ""
"**Samo pisanje v priklopne datoteke XMP** se ne bo dotaknilo metapodatkov "
"elementa."

#: ../../setup_application/metadata_settings.rst:71
msgid ""
"**Write to item and XMP Sidecar** will touch both item and sidecar at the "
"same time."
msgstr ""
"**Pišite v element in priklopno datoteko XMP Sidecar** se bo hkrati dotaknil "
"elementa in priklopne datoteke."

#: ../../setup_application/metadata_settings.rst:72
msgid ""
"**Write to XMP sidecar for read-only item only** will handle sidecar for non-"
"writable items only, as video or RAW files for example."
msgstr ""
"**Pisanje v priklopne datoteke XMP samo za elemente, ki so samo za branje** "
"bo obravnavalo priklopne datoteke samo za nezapisljive elemente, kot so na "
"primer videoposnetki ali surove datoteke RAW."

#: ../../setup_application/metadata_settings.rst:74
msgid ""
"Filename for the sidecars set to :file:`filename.ext.xmp`. For example, :"
"file:`image1.dng` will have a sidecar file named :file:`image1.dng.xmp`. "
"With the option **Sidecar file names are compatible with commercial "
"programs** digiKam will create the XMP sidecar files with a compatible file "
"name (:file:`image1.xmp`) used by many commercial programs."
msgstr ""
"Ime datoteke za priklopne datoteke je nastavljeno na :file:`imedatoteke.ext."
"xmp`. Na primer: :file:`image1.dng` bo imela priklopno datoteko z imenom :"
"file:`image1.dng.xmp`. Z možnostjo **Imena priklopnih datotek so združljiva "
"s komercialnimi programi** bo digiKam ustvaril priklopne datoteke XMP z "
"združljivo datoteko imena (:file:`image1.xmp`), ki ga uporabljajo številni "
"komercialni programi."

#: ../../setup_application/metadata_settings.rst:80
msgid "The digiKam Metadata Sidecar Behavior Settings Page"
msgstr "Stran z nastavitvami vedenja priklopnih datotek z metapodatki digiKama"

#: ../../setup_application/metadata_settings.rst:84
msgid ""
"If the box **Read from sidecar files** is checked, digiKam will only read "
"the sidecar while ignoring the embedded metadata."
msgstr ""
"Če je označeno polje **Preberi iz priklopnih datotek**, bo digiKam prebral "
"samo priklopno datoteko, medtem ko ne bo upošteval vdelanih metapodatkov."

#: ../../setup_application/metadata_settings.rst:86
msgid ""
"The option **Additional sidecar file extensions** allows to add extra "
"filename extensions to be processed alongside regular items, independently "
"of the XMP sidecars. These files will be hidden, but regarded as an "
"extension of the main file. Just write :file:`thm pp3` to support :file:"
"`filename.thm` (extra Jpeg thumbnail for RAW) and :file:`filename.pp3` "
"(RAWTheraPee metadata) sidecars."
msgstr ""
"Možnost **Dodatne razširitve priklopnih datotek** omogoča dodajanje dodatnih "
"končnic imen datotek, ki se obdelujejo skupaj z običajnimi elementi "
"neodvisnih priklopnih datotek XMP. Te datoteke bodo skrite, vendar bodo "
"obravnavane kot razširitev glavne datoteke. Samo napišite :file:`thm pp3` za "
"podporo :file:`filename.thm` (dodatna sličica Jpeg za RAW) in :file:"
"`filename.pp3` (RAWTheraPee metapodatki) priklopne datoteke."

#: ../../setup_application/metadata_settings.rst:91
msgid "Rotation Settings"
msgstr "Nastavitve vrtenja"

#: ../../setup_application/metadata_settings.rst:97
msgid "The digiKam Metadata Rotation Behavior Settings Page"
msgstr "Stran z nastavitvami vedenja vrtenja metapodatkov digiKam"

#: ../../setup_application/metadata_settings.rst:99
msgid ""
"**Show images/thumbnails rotated according to orientation tag**: this will "
"use any orientation information that your camera has included in the Exif "
"information to automatically rotate your photographs so that they are the "
"correct way up when displayed. It will not actually rotate the image file, "
"only the display of the image on the screen. If you want to permanently "
"rotate the image on file, you can click with the right mouse button on the "
"thumbnail and select **Auto-rotate/flip according to Exif orientation**. The "
"image will then be rotated on disk and the tag will be reset to \"normal\". "
"If your camera routinely gets this orientation information wrong you might "
"like to switch this feature off."
msgstr ""
"**Prikaži slike/sličice, obrnjene glede na orientacijsko značko**: to bo "
"uporabilo vse informacije o orientaciji, ki jih je vaša kamera vključila v "
"informacije Exif za samodejno vrtenje vaših fotografij, tako da so obrnjene "
"navzgor, ko so prikazane. Slikovne datoteke dejansko ne bo zavrtel, ampak "
"samo prikaz slike na zaslonu. Če želite trajno zavrteti sliko v datoteki, "
"lahko z desnim gumbom miške kliknete na sličico in izberite **Samodejno "
"zavrti/obrni glede na usmerjenost Exif**. Slika bo nato zavrtena na disku in "
"značka bo ponastavljena na \"normalno\". Če vaša kamera redno dobiva te "
"podatke o orientaciji napačno, bi morda želite izklopiti to možnost."

#: ../../setup_application/metadata_settings.rst:101
msgid ""
"**Set orientation tag to normal after rotate/flip**: the auto-rotate option "
"automatically corrects the orientation of images taken with digital cameras "
"that have an orientation sensor. The camera adds an orientation tag to the "
"image's Exif metadata. digiKam can read this tag to adjust the image "
"accordingly. If you manually rotate an image, these metadata will be "
"incorrect. This option will set the orientation tag to *Normal* after an "
"adjustment, assuming that you rotated it to the correct orientation. Switch "
"this off if you don't want digiKam to make changes to the orientation tag, "
"when you rotate or flip the image."
msgstr ""
"**Nastavi orientacijsko značko na običajno po vrtenju/obračanju**: možnost "
"samodejnega vrtenja samodejno popravi orientacijo slik, posnetih z "
"digitalnimi kamerami, ki imajo orientacijski senzor. Kamera doda "
"orientacijsko oznako v metapodatke Exif slike. digiKam lahko prebere to "
"značko in temu primerno prilagodi. Če ročno zavrtite sliko, bodo ti "
"metapodatki nepravilni. Ta možnost bo nastavila orientacijsko značko na "
"*Normal* po nastavitvi, ob predpostavki, da ste ga zavrteli v pravilno smer. "
"Izklopite to stikalo, če ne želite, da digiKam spreminja orientacijsko "
"značko, ko zavrtite ali obrnete sliko."

#: ../../setup_application/metadata_settings.rst:106
msgid "Views Settings"
msgstr "Nastavitve pogleda"

#: ../../setup_application/metadata_settings.rst:108
msgid ""
"These settings allows to customize the metadata contents displayed in Exif, "
"Makernotes, IPTC, XMP, and ExifTool viewers from the right sidebar. For more "
"details see :ref:`this section <metadata_view>` from the manual."
msgstr ""
"Te nastavitve omogočajo prilagoditev vsebine metapodatkov, prikazanih v "
"Exifu,Makernotes, IPTC, XMP in ExifTool pregledovalnikih v desnem stranskem "
"stolpcu. Za več podrobnosti glejte :ref:`ta razdelek <metadata_view>` v "
"priročniku."

#: ../../setup_application/metadata_settings.rst:114
msgid "The digiKam Settings For The Metadata Viewers"
msgstr "Nastavitve digiKam za pregledovalnike metapodatkov"

#: ../../setup_application/metadata_settings.rst:119
msgid "ExifTool Settings"
msgstr "Nastavitve ExifTool"

#: ../../setup_application/metadata_settings.rst:121
msgid ""
"`ExifTool <https://exiftool.org/>`_ is a backend engine that digiKam can use "
"to process operations on metadata, as view, read, and write. This panel only "
"show the detection of the ExifTool binary program, and the supported formats "
"with the respective read and write features."
msgstr ""
"`ExifTool <https://exiftool.org/>`_ je zaledni mehanizem, ki ga lahko "
"digiKam uporablja za obdelavo operacij na metapodatkih, kot so ogled, branje "
"in pisanje. Ta plošča prikazuje samo zaznavanje binarnega programa ExifTool "
"in podprte formate z ustreznimi funkcijami za branje in pisanje."

#: ../../setup_application/metadata_settings.rst:127
msgid "The digiKam Settings For The ExifTool Backend"
msgstr "Nastavitve digiKam za zaledje ExifTool"

#: ../../setup_application/metadata_settings.rst:131
msgid ""
"To replace the Exiv2 backend by ExifTool with all read and write metadata "
"operations, see the :ref:`Metadata Behavior <metadata_behavior>` section of "
"this manual."
msgstr ""
"Če želite zaledje Exiv2 zamenjati z ExifTool z vsemi operacijami branja in "
"pisanja metapodatkov, glejte razdelek :ref:`Vedenje metapodatkov "
"<metadata_behavior>` v tem priročniku."

#: ../../setup_application/metadata_settings.rst:136
msgid "Baloo Settings"
msgstr "Baloo nastavitve"

#: ../../setup_application/metadata_settings.rst:138
msgid ""
"**Baloo** is the file indexing and file search framework for **KDE Plasma** "
"under Linux, with a focus on providing a very small memory footprint along "
"with an extremely fast searching. Baloo is not an application, but a daemon "
"to index files."
msgstr ""
"**Baloo** je ogrodje za indeksiranje datotek in iskanje datotek za **KDE "
"Plasma** pod Linuxom, s poudarkom na zagotavljanju zelo majhnega "
"pomnilniškega odtisa z izjemno hitrim iskanjem. Baloo ni aplikacija, ampak "
"demon za indeksiranje datotek."

#: ../../setup_application/metadata_settings.rst:144
msgid "The digiKam Settings For The Baloo Metadata Search Engine"
msgstr "Nastavitve digiKam za iskalnik metapodatkov Baloo"

#: ../../setup_application/metadata_settings.rst:146
msgid ""
"This page allows to share metadata stored in digiKam database with the Baloo "
"search engine. Extra applications as **KDE Dolphin** file manager can use "
"the Baloo interface to provide file search results with items managed by the "
"digiKam database."
msgstr ""
"Ta stran omogoča skupno rabo metapodatkov, shranjenih v bazi podatkov "
"digiKam, z iskalnikom Baloo. Dodatne aplikacije, kot jih upravljalnik "
"datotek **KDE Dolphin** lahko uporablja vmesnik Baloo za zagotavljanje "
"rezultatov iskanja datotek z elementi, ki jih upravlja baza podatkov digiKam."

#: ../../setup_application/metadata_settings.rst:150
msgid ""
"This page is only available under Linux, not Windows and macOS. The KDE "
"Plasma **Files Indexer** feature must be enabled in the KDE Plasma control "
"Panel."
msgstr ""
"Ta stran je na voljo samo v sistemu Linux, ne pa v sistemu Windows in macOS. "
"KDE Plasma **Indeksirnik datotek** mora biti omogočena na plošči kontrolnika "
"KDE Plasma."

#: ../../setup_application/metadata_settings.rst:155
msgid "Advanced Settings"
msgstr "Napredne nastavitve"

#: ../../setup_application/metadata_settings.rst:157
msgid ""
"The **Advanced** tab allows you to manage namespaces used by digiKam to "
"store and retrieve tags, ratings and comments. This functionality is often "
"used by advanced users to synchronize metadata between different software. "
"Please leave the default settings if you are not sure what to do here."
msgstr ""
"Zavihek **Napredno** vam omogoča upravljanje imenskih prostorov, ki jih "
"digiKam uporablja za shranjevanje in pridobivanje značk, ocen in "
"komentarjev. To funkcijo pogosto uporabljajo napredni uporabniki za "
"sinhronizacijo metapodatkov med različno programsko opremo. Pustite privzete "
"nastavitve, če niste prepričani, kaj storiti tukaj."

#: ../../setup_application/metadata_settings.rst:159
msgid "The categories that you can manage with these advanced settings are:"
msgstr ""
"Kategorije, ki jih lahko upravljate s temi naprednimi nastavitvami, so:"

#: ../../setup_application/metadata_settings.rst:161
msgid ""
"**Caption**: all languages-alternative comments (supported by XMP only), "
"else the simple comments values (Exif and IPTC)."
msgstr ""
"**Napis**: vsi jeziki-alternativni komentarji (podprto samo z XMP), sicer "
"preproste vrednosti komentarjev (Exif in IPTC)."

#: ../../setup_application/metadata_settings.rst:162
msgid ""
"**Color Label**: the color labels properties to apply on items in your "
"workflow."
msgstr ""
"**Barvna oznaka**: lastnosti barvnih oznak, ki jih želite uporabiti na "
"elementih v vašem delovnem toku."

#: ../../setup_application/metadata_settings.rst:163
msgid "**Rating**: the stars properties to apply on items in your workflow."
msgstr ""
"**Ocena**: lastnosti zvezdic, ki jih je treba uporabiti za elemente v vašem "
"delovnem toku."

#: ../../setup_application/metadata_settings.rst:164
msgid ""
"**Tags**: the nested keywords hierarchy to apply on items in your workflow "
"(supported by XMP only), else the simple flat list of keywords (Exif and "
"IPTC)."
msgstr ""
"**Značke**: hierarhija ugnezdenih ključnih besed, ki se uporabijo za "
"elemente v vašem delovnem toku (podprto samo z XMP), drugače preprost seznam "
"ključnih besed (Exif in IPTC)."

#: ../../setup_application/metadata_settings.rst:165
msgid ""
"**Title**: all languages-alternative titles (supported by XMP only), else "
"the simple title values (Exif and IPTC)."
msgstr ""
"**Naslov**: vsi jeziki-nadomestni naslovi (podprti samo z XMP), drugače "
"preproste vrednosti naslova (Exif in IPTC)."

#: ../../setup_application/metadata_settings.rst:167
msgid ""
"For each category you can set the read and write behavior in metadata. The "
"default settings is to **Unify Read and Write** operations, but if you "
"disable this option, you can customize **Read Options** and **Write "
"Options** independently."
msgstr ""
"Za vsako kategorijo lahko nastavite vedenje branja in pisanja v "
"metapodatkih. Privzeta nastavitev je **Poenotenje branja in pisanja**, "
"vendar če onemogočite to možnost, lahko prilagodite **Možnosti branja** in "
"**Možnosti pisanja** neodvisno."

#: ../../setup_application/metadata_settings.rst:173
msgid "The digiKam Advanced Metadata Settings For the **Caption** Category"
msgstr "Napredne nastavitve metapodatkov digiKam za kategorijo **Caption**"

#: ../../setup_application/metadata_settings.rst:175
msgid ""
"On this example, the top **Caption** entry in the list is **Xmp.dc."
"description**, and it will be read by digiKam first. If it contains a valid "
"value it will be used, otherwise the next entry named **Xmp.exif."
"UserComment**, etc. The entries list priority is high on the top and low on "
"the bottom. The entry in the list are used only if item is enabled with the "
"checkbox preceding the name."
msgstr ""
"V tem primeru je zgornji vnos **Caption** na seznamu **Xmp.dc.description** "
"in najprej ga bo prebral digiKam. Če vsebuje veljavno vrednost, ki bo "
"uporabljena, sicer bo naslednji vnos z imenom **Xmp.exif.UserComment** itd. "
"Prednost seznama vnosov je visoko na vrhu in nizko na dnu. Vnos na seznamu "
"se uporablja samo, če je element omogočen s potrditvenim poljem pred imenom."

#: ../../setup_application/metadata_settings.rst:177
msgid "With the buttons on the right side, you can customize the list:"
msgstr "Z gumbi na desni strani lahko prilagodite seznam:"

#: ../../setup_application/metadata_settings.rst:179
msgid "**Add**: allows to add a new entry in the tags list."
msgstr "**Dodaj**: omogoča dodajanje novega vnosa v seznam značk."

#: ../../setup_application/metadata_settings.rst:180
msgid "**Edit**: allows to modify the current select entry in the list."
msgstr "**Uredi**: omogoča spreminjanje trenutnega izbranega vnosa na seznamu."

#: ../../setup_application/metadata_settings.rst:181
msgid "**Delete**: allows to remove the current select entry in the list."
msgstr "**Izbriši**: omogoča odstranitev trenutnega izbranega vnosa s seznama."

#: ../../setup_application/metadata_settings.rst:182
msgid ""
"**Move up**: allows to move the current select entry in the list to a higher "
"priority."
msgstr ""
"**Premakni navzgor**: omogoča premik trenutnega izbranega vnosa na seznamu "
"na višjo prednost."

#: ../../setup_application/metadata_settings.rst:183
msgid ""
"**Move Down**: allows to move the current selected entry in the list to a "
"lower priority."
msgstr ""
"**Premakni navzdol**: omogoča premik trenutno izbranega vnosa na seznamu na "
"nižjo prioriteto."

#: ../../setup_application/metadata_settings.rst:184
msgid "**Revert Changes**: allows to revert last changes done on the list."
msgstr ""
"**Povrni spremembe**: omogoča razveljavitev zadnjih opravljenih sprememb na "
"seznamu."

#: ../../setup_application/metadata_settings.rst:185
msgid ""
"**Save Profile**: allows to save the current configuration to a **Profile** "
"file."
msgstr ""
"**Shrani profil**: omogoča shranjevanje trenutne konfiguracije v datoteko "
"**Profil**."

#: ../../setup_application/metadata_settings.rst:186
msgid ""
"**Load Profile**: allows to load a saved configuration from a **Profile** "
"file."
msgstr ""
"**Naloži profil**: omogoča nalaganje shranjene konfiguracije iz datoteke "
"**Profil**."

#: ../../setup_application/metadata_settings.rst:187
msgid ""
"**Revert To Default**: allows to reset the current list to the default "
"values."
msgstr ""
"**Povrni na privzeto**: omogoča ponastavitev trenutnega seznama na privzete "
"vrednosti."

#: ../../setup_application/metadata_settings.rst:189
msgid ""
"The **Profile** are simple ini-based text file used to store the advanced "
"metadata settings to the disk. A profile can be loaded to overload the "
"current configuration, depending of your workflow and the rules to apply for "
"the best interoperability with other photo management programs. digiKam "
"comes with a compatibility profile for **DarkTable**."
msgstr ""
"**Profil** je preprosta besedilna datoteka na osnovi ini, ki se uporablja za "
"shranjevanje naprednih nastavitve metapodatkov na disk. Profil lahko "
"naložite, da preobremenitetrenutno konfiguracijo, odvisno od vašega poteka "
"dela in pravil, za katera želite zagotoviti kar najboljšo interoperabilnost "
"z drugimi programi za upravljanje fotografij. digiKam prihaja s profilom "
"združljivosti za **DarkTable**."

#: ../../setup_application/metadata_settings.rst:193
msgid ""
"We recommend to always put XMP tags to the top priority on this list, as XMP "
"has better features than IPC and Exif."
msgstr ""
"Priporočamo, da značke XMP vedno postavite na prvo mesto na tem seznamu, saj "
"ima XMP boljše funkcije kot IPC in Exif."

#: ../../setup_application/metadata_settings.rst:197
msgid ""
"The **Tags** category provide an extra option named **Read All Metadata For "
"Tags** to force operations on all the namespaces."
msgstr ""
"Kategorija **Značke** ponuja dodatno možnost, imenovano **Preberi vse "
"metapodatke za značke** za vsiljevanje operacij v vseh imenskih prostorih."

#~ msgid "The information to record in files metadata are listed below:"
#~ msgstr "Podatki za zapis v metapodatke datotek so navedeni spodaj:"

#~ msgid ""
#~ "**Rating** will store the rate of the contents. Usualy this information "
#~ "is stored in Exif and XMP. Usually this information are stored in Exif "
#~ "and XMP."
#~ msgstr ""
#~ "**Ocena** bo shranila oceno vsebine. Običajno so te informacije shranjene "
#~ "v Exif in XMP."

#~ msgid "**Rating** will store the rate of the contents."
#~ msgstr "**Ocena** bo shranila oceno vsebine."
