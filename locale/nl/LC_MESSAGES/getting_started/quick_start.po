# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-01-11 11:43+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../getting_started/quick_start.rst:1
msgid "How to quickly start digiKam photo management program"
msgstr "Hoe het fotobeheerprogramma digiKam snel opstarten"

#: ../../getting_started/quick_start.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, first-run, scan, setup"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, eerste keer. scannen, instellen"

#: ../../getting_started/quick_start.rst:14
msgid "Quick Start"
msgstr "Snelstart"

#: ../../getting_started/quick_start.rst:20
msgid "digiKam First Run Assistant Welcome Page"
msgstr "Welkomstpagina van de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:22
msgid ""
"When you start digiKam for the very first time it will ask you where you "
"store your photographs. You can choose any local, remote or removable "
"folder. Just type in the path name of a folder or click on the **Browse...** "
"icon to select a folder from the dialog."
msgstr ""
"Wanneer u digiKam voor de allereerste keer start zal het vragen waar u uw "
"foto's en de digiKam-database wilt opslaan. U kunt elke map lokaal, op "
"afstand of verwijderbaar kiezen. Typ gewoon de padnaam van een map in of "
"klik op het pictogram **Bladeren** om een map met de dialoog te kiezen."

#: ../../getting_started/quick_start.rst:24
msgid ""
"Later on you can add as many locations as you like - digiKam will add them "
"to the album library. Look menu entry :menuselection:`Settings --> Configure "
"digiKam --> Collections`"
msgstr ""
"Later kunt u zoveel locaties als u wilt toevoegen - digiKam zal ze toevoegen "
"aan de albumbibliotheek. Kijk met het menu-item :menuselection:`Instellingen "
"--> digiKam configureren --> Verzamelingen`"

#: ../../getting_started/quick_start.rst:30
msgid "digiKam First Run Assistant Collection Path"
msgstr "Pad met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:32
msgid ""
"Then you have to specify a local folder that resides on your computer, in "
"which the database will be stored. This path will be the same for all image "
"folders. For details about the database type selection have a look at :ref:"
"`the digiKam Database <database_intro>`. If you have a local installation "
"and a collection of less than 100.000 photographs you may well keep the "
"default setting (SQLite)."
msgstr ""
"U moet dan een lokale map op uw computer specificeren, waarin de database "
"zal worden opgeslagen`. Dit pad zal hetzelfde zijn voor alle mappen met "
"afbeeldingen. Voor details over de selectie van het type database moet u "
"kijken in :ref:`De database van digiKam <database_intro>`. Als u een lokale "
"installatie en een verzameling van minder dan 30,000 foto's hebt dan kunt u "
"de standaard instelling (SQLite) behouden."

#: ../../getting_started/quick_start.rst:38
msgid "digiKam First Run Assistant Database Setup"
msgstr "Instellingen met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:40
msgid ""
"Next, you will have to choose the way you would like to open RAW files : "
"either with automatic adjustments or with the digiKam RAW import tool to "
"adjust corrections manually. If you don't know what is a RAW file or if your "
"camera doesn't support RAW files, you should keep the default setting and "
"skip this step."
msgstr ""
"Vervolgens moet u de manier kiezen waarop u RAW-bestanden wilt openen : "
"ofwel met automatische aanpassingen of met het RAW-importhulpmiddel van "
"digiKam om correcties handmatig aan te passen. Als u niet weet wat een RAW-"
"bestand is of als uw camera geen RAW-bestanden ondersteunt, dan zou u de "
"standaard instelling moeten behouden en deze stap overslaan."

#: ../../getting_started/quick_start.rst:47
msgid "digiKam First Run Assistant Raw File Behavior"
msgstr "Gedrag van RAW-bestand met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:49
msgid ""
"Metadata information storage may be important if you plan to work with "
"another photo management program and you should choose **Add information to "
"files**. But if you don't want to alter your pictures, keep the default "
"setting: **Do nothing**."
msgstr ""
"Opslag van metagegevensinformatie kan belangrijk zijn als u van plan bent "
"met een ander fotobeheertoepassing te werken en zou u **Informatie aan "
"bestanden toevoegen** moeten kiezen. Maar als u uw afbeeldingen niet wilt "
"veranderen, behoudt dan de standaard instelling: **Niets doen**."

#: ../../getting_started/quick_start.rst:55
msgid "digiKam First Run Assistant Metadata Behavior"
msgstr "Gedrag van metagegevens met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:57
msgid ""
"Then choose how digiKam will load images in preview mode and light table. "
"Reduced version will load faster but at the cost of quality."
msgstr ""
"Kies daarna hoe digiKam afbeeldingen in voorbeeldmodus en lichttafel moet "
"laden. Een gereduceerde versie zal sneller laden maar ten koste gaan van de "
"kwaliteit."

#: ../../getting_started/quick_start.rst:63
msgid "digiKam First Run Assistant Preview Behavior"
msgstr "Gedrag van voorbeelden met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:65
msgid ""
"Now decide how digiKam will open images with a click of the right mouse "
"button. Previews will load faster but you won't be able to make any "
"corrections."
msgstr ""
"Beslis nu hoe digiKam afbeeldingen zal openen met een klik van de rechter "
"muisknop. Voorbeelden zullen sneller laden maar u kunt geen correcties maken."

#: ../../getting_started/quick_start.rst:71
msgid "digiKam First Run Assistant Open-File Behavior"
msgstr "Gedrag van bestand openen met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:73
msgid ""
"Tooltips are a fast and easy way to display important information about a "
"photograph, they popup as the mouse hovers over a thumbnail. Select **Use "
"tooltip** if you want to display them."
msgstr ""
"Tekstballonnen zijn een snelle en gemakkelijke manier om belangrijke "
"informatie over een foto te tonen, ze verschijnen als de muis over een "
"miniatuur zweeft. Selecteer **Tekstballonnen gebruiken** als u ze wilt tonen."

#: ../../getting_started/quick_start.rst:79
msgid "digiKam First Run Assistant Tooltips Behavior"
msgstr "Gedrag van tekstballonnen met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:81
msgid "Click on **Finish**, digiKam will now start to scan for photographs..."
msgstr ""
"Klik op **Beëindigen**. digiKam zal nu beginnen met het zoeken naar foto's..."

#: ../../getting_started/quick_start.rst:87
msgid "digiKam First Run Assistant Last Page"
msgstr "Laatste pagina van de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:89
msgid ""
"Nothing really to select here. You can cancel or start the scan with "
"**Finish**. None of the photographs will be altered. During folders parsing "
"you can see a progress bar in the lower right corner like this:"
msgstr ""
"Hier is niet te selecteren. U kunt het zoeken annuleren of starten met "
"**Beëindigen**. Geen van de foto's zelf zullen worden gewijzigd. Tijdens het "
"scannen van de mappen ziet u een voortgangsbalk in de rechtsonder hoek zoals "
"deze:"

#: ../../getting_started/quick_start.rst:95
msgid "digiKam First Start Scan Process to Populate The Database"
msgstr "Het eerste zoek/scanproces met de assistent van digiKam Eerste keer"

#: ../../getting_started/quick_start.rst:99
msgid ""
"digiKam will recurse the library path to its full depth. You cannot exclude "
"(prune) any sub-path unless you make that sub-path hidden. You have to do "
"that from outside of digiKam by putting a dot in front of the sub-path. "
"Later in digiKam configuration panel, you can list folders to exclude with "
"the :ref:`Ignored Directories <ignored_directories>` settings."
msgstr ""
"digiKam zal recursief het bibliotheekpad tot zijn volledige diepte "
"doorlopen. U kunt geen sub-pad uitsluiten (prune) tenzij u dat sub-pad "
"verbergt. U moet dat buiten digiKam doen door een punt vóór het sub-pad te "
"zetten. Later in het configuratiepaneel kunt u een lijst van mappen maken om "
"uit te sluiten met de instelling :ref:`Genegeerde mappen "
"<ignored_directories>`."

#: ../../getting_started/quick_start.rst:101
msgid ""
"As digiKam uses your folders on your hard disk directly, other applications "
"like file managers can remove any albums outside a digiKam session. In this "
"case digiKam will ask you at the next session whether all albums that have "
"been removed from the digiKam photographs root path shall be deleted from "
"albums database. If you want to move folders around and don't want to do "
"that in digiKam, we suggest you do that while digiKam is running, so the "
"database will be kept in sync and you do not lose any metadata."
msgstr ""
"Omdat digiKam uw mappen op de harde schijf als albums gebruikt, kunnen "
"andere programma's, zoals bestandsbeheerders, buiten digiKam om albums "
"verplaatsen of verwijderen. In dat geval zal digiKam bij de volgende start u "
"vragen of alle albums die uit de albumbibliotheek van digiKam zijn "
"verwijderd moeten worden weggehaald uit de database. Als u mappen wilt "
"verplaatsen, maar dat liever niet in digiKam zelf wilt doen, dan adviseren "
"wij u om dat te doen terwijl digiKam nog draait. Op die manier kan het "
"programma er voor zorgen dat de database op de wijziging wordt aangepast en "
"er geen metagegevens verloren gaan."

#: ../../getting_started/quick_start.rst:103
msgid ""
"When you use an existing folder of photographs as the Album Library folder, "
"you will notice that the Albums in the Album list do not have photographs as "
"their icons. You can change that by dragging any photograph in the Album "
"onto the folder icon in the left sidebar and use this as the Album icon. "
"See :ref:`the Album section <albums_view>` for details of how to change the "
"Album icon."
msgstr ""
"Wanneer u een bestaande map met foto's gebruikt als albummap, dan zult u "
"zien dat het albums in de albumlijst geen foto's als pictogrammen heeft. U "
"kunt dat veranderen door een foto in het album naar het map-pictogram te "
"slepen in de linker zijbalk en die als het album-pictogram te gebruiken. "
"Zie :ref:`de album-sectie <albums_view>` voor details van hoe het album-"
"pictogram te wijzigen."

#: ../../getting_started/quick_start.rst:105
msgid ""
"digiKam uses a dedicated database to store thumbnails with an optimized "
"wavelets compression algorithm (PGF). There is no way of hiding non-standard "
"thumbnail folders from the **Albums** list. If you want to keep them you "
"could create an Album Category that just contains all the thumbnail Folders "
"and then view your Albums in :menuselection:`View --> Sort Albums --> By "
"Category` order. See :ref:`the Album section <albums_view>` for more about "
"Album Categories."
msgstr ""
"digiKam gebruikt een eigen database om miniaturen met een geoptimaliseerd "
"wavelets-compressie-algoritme (PGF) op te slaan. Er is geen manier om niet-"
"standaard mappen voor miniaturen van de lijst **Albums** te verbergen. Als u "
"ze wilt behouden dan zou u een Albumcategorie moeten aanmaken die alleen "
"alle mappen met miniaturen bevat en dan uw Albums op volgorde bekijken in :"
"menuselection:`Beeld -->Albums sorteren --> Op categorie`. Zie de :ref:"
"`Albumsectie <albums_view>` voor meer over Albumcategorieën."

#: ../../getting_started/quick_start.rst:107
msgid ""
"Once you have configured the Album Library Folder you can set up digiKam to "
"work with your digital camera and then learn how to use :ref:`the Album "
"section <albums_view>` and :ref:`the Tags section <tags_view>` to arrange "
"your photograph Albums."
msgstr ""
"Nadat u de albumbibliotheekmap hebt ingesteld kunt u digiKam instellen om "
"met uw digitale camera samen te werken en daarna te leren hoe :ref:`de "
"Albumsectie<albums_view>` en :ref:`de Tagssectie <tags_view>` te gebruiken "
"om uw fotoalbums in te delen."

#: ../../getting_started/quick_start.rst:113
msgid ""
"At first run, digiKam will ask to download large files from Internet. These "
"files are the deep-learning models used for the face management, red eyes "
"removal, and the image quality sorting features. If you pass this stage "
"without downloading files, these features will not work properly, but you "
"can reload this file later using option from the :ref:`Setup/Miscs/System "
"<system_settings>` dialog page."
msgstr ""
"De eerste keer zal digiKam vragen om grote bestanden uit het internet te "
"downloaden. Deze bestanden zijn de deep-learning modellen gebruikt voor het "
"beheer van gezichten, verwijderen van rode ogen en de functies voor sorteren "
"op afbeeldingskwaliteit. Als u deze stap overslaat zonder de bestanden te "
"downloaden, zullen deze functies niet juist werken, maar u kunt dit bestand "
"later opnieuw laden met de optie uit de dialoogpagina :ref:`Instellen/"
"Diversen/Systeem <system_settings>`."

#: ../../getting_started/quick_start.rst:119
msgid "digiKam First Run Dialog to Download Deep-learning Model Files"
msgstr ""
"Dialoog om deep-learningmodelbestanden te downloaden met de assistent van "
"digiKam Eerste keer"
